using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class ArchivosDeGuardado : MonoBehaviour
{
    public int punt;
    public PersistenciaSO persistente;

    public string ArchivoGuardado;
    public Puntuacion datos = new Puntuacion();

    private void Start()
    {
        ArchivoGuardado = Application.dataPath + "/LightworldSafeData.json";
        bool funcionado = Cargar();
        if (!funcionado) Cargar();
        if (datos.punt < persistente.punt) 
        {
            datos.punt = persistente.punt;
            Guardar(); 
            Cargar(); 
        }
        this.GetComponent<TextMeshProUGUI>().text = datos.punt + "";
    }

    public bool Cargar()
    {
        if (File.Exists(ArchivoGuardado))
        {
            string contenido = File.ReadAllText(ArchivoGuardado);
            datos = JsonUtility.FromJson<Puntuacion>(contenido);
            print("Puntuacion: " + datos.punt);
            return true;
        }
        else
        {
            Guardar();
            return false;
        }
    }

    public void Guardar()
    {
        string cadena = JsonUtility.ToJson(datos);
        File.WriteAllText(ArchivoGuardado, cadena);
        print("Archivo guardado");
    }
}
