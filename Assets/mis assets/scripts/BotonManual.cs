using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonManual : MonoBehaviour
{
    public Manual manual;

    public void AumentarImagen()
    {
        manual.CambiarImagen(1);
    }

    public void DisminuirImagen()
    {
        manual.CambiarImagen(-1);
    }
}
