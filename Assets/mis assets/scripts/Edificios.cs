
using UnityEngine.Animations;
using UnityEngine;


[CreateAssetMenu]
public class EdificiosSO : ScriptableObject
{
    public string Nombre;
    public string Tipo;
    public bool rompible; // el jugador lo puede romper
    public bool destruible; // los enemigos le hacen da�o al edificio en vez el planeta
    public int dmg; // el da�o que hace si ataca
    public GameObject municion; // la municion que utiliza si dispara
    public int vida; // la vida que tiene en caso de que destruible sea true
    public string comp; // el comportamiento extra que tiene en caso de que sea un edificio de apoyo
    public Sprite sprite; // sprite para cuando esta generado
    public RuntimeAnimatorController animator; // en caso de ser un recurso, en vez de ser un sprite sera un animator para dejarlo crecer poco a poco
    public Sprite SPifVacio; // sprite para cuando no esta generado
    public RuntimeAnimatorController ANifVacio; // animator para cuando no esta generado
    public int CosteMadera; // la cantidad de madera que se necesita para comprarlo en caso de que se pueda comprar
    public int CostePiedra; // la cantidad de piedra que se necesita para comprarlo en caso de que se pueda comprar
    public int CosteHierro; // la cantidad de hierro que se necesita para comprarlo en caso de que se pueda comprar
}
