using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EjercitoSO : ScriptableObject
{
    public string nombre;
    public int vida;
    public GameObject municion;
    public float shootSpd;
    public string tipo;
    public int dmg;
    public int punt;
    // tipos permitidos:
    // recto: enemigo basico simplemente dispara cada x segundos
    // rapido: enemigo que dispara rafagas de disparos
    // ninja: aparece y desaparece cada x segundo (aparece para disparar)
    // de carga: genera enemigos
}