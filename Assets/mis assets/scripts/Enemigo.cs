using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public int vida;
    public EjercitoSO ejercitoTipo;
    public Sprite none;
    public RuntimeAnimatorController noneAN;
    public bool muerto;
    public bool enArea;
    public bool enCorutine;
    public bool visible;
    public Planeta planeta;
    public float mult;
    public int vueltas;
    public proyectil[] municiones;
    public PersistenciaSO persistente;

    // Sonidos gestor de audio: 0 disparar 1 dolor

    void Start()
    {
        vida = ejercitoTipo.vida * vueltas;
        muerto = false;
        enArea = false;
        enCorutine = false;
        visible = true;
        transform.up = (planeta.transform.position - transform.position).normalized;
        mult = 1;
        if (this.ejercitoTipo.tipo == "de carga") mult = 1.2f;
    }

    void Update()
    {
        if (vida <= 0)
        {
            if (!muerto)
            {
                this.persistente.punt += this.ejercitoTipo.punt;
            }
            muerto = true;
            if (!this.GetComponent<SpriteRenderer>().isVisible)
            {
                this.GetComponent<Animator>().runtimeAnimatorController = noneAN;
                this.GetComponent<SpriteRenderer>().sprite = none;
            }
            else
            {
                transform.Translate(transform.up * 0.05f, Space.World);
                Vector3 direction = (planeta.transform.position - transform.position).normalized;
                transform.up = direction;
                this.transform.Rotate(new Vector3(0, 0, 180));
            }
        }
        else
        {
            Vector3 direction = (planeta.transform.position - transform.position).normalized;
            transform.up = direction;
            this.transform.Rotate(new Vector3(0, 0, 180));
            if (enArea)
            {
                if (!enCorutine)
                {
                    StartCoroutine(disparar());
                    enCorutine = true;
                }
                if (!planeta.objeto.activeSelf) this.transform.RotateAround(planeta.transform.position, new Vector3(0, 0, 1), 0.7f*mult);
            }
            else if (!planeta.objeto.activeSelf) transform.Translate(direction * 0.05f, Space.World);
        }
    }

    public IEnumerator disparar()
    {
        if (this.ejercitoTipo.tipo == "recto")
        {
            while (true)
            {
                yield return new WaitForSeconds(ejercitoTipo.shootSpd);
                if (muerto == true) break;
                GameObject bala = ejercitoTipo.municion;
                for (int l = 0; l < municiones.Length; l++)
                {
                    if (!municiones[l].gameObject.activeInHierarchy)
                    {
                        bala = municiones[l].gameObject;
                        bala.GetComponent<proyectil>().bueno = false;
                        bala.GetComponent<proyectil>().dmg = this.ejercitoTipo.dmg + 1 * (this.vueltas - 1);
                        bala.transform.position = this.transform.position;
                        bala.SetActive(true);
                        bala.GetComponent<proyectil>().disparar();
                        this.GetComponent<GestorAudio>().reproducirAudio(0, 10);
                        break;
                    }
                }
            }
        }
        else if (this.ejercitoTipo.tipo == "rapido")
        {
            while (true)
            {
                yield return new WaitForSeconds(ejercitoTipo.shootSpd * 10);
                for (int i = 0; i < 5; i++)
                {
                    if (muerto == true) break;
                    GameObject bala = ejercitoTipo.municion;
                    for (int l = 0; l < municiones.Length; l++)
                    {
                        if (!municiones[l].gameObject.activeInHierarchy)
                        {
                            bala = municiones[l].gameObject;
                            bala.GetComponent<proyectil>().bueno = false;
                            bala.GetComponent<proyectil>().dmg = this.ejercitoTipo.dmg + 1 * (this.vueltas - 1);
                            bala.transform.position = this.transform.position;
                            bala.SetActive(true);
                            bala.GetComponent<proyectil>().disparar();
                            this.GetComponent<GestorAudio>().reproducirAudio(0, 10);
                            break;
                        }
                    }
                    yield return new WaitForSeconds(ejercitoTipo.shootSpd);
                }
            }
        }
        else if (this.ejercitoTipo.tipo == "ninja")
        {
            while (true)
            {
                yield return new WaitForSeconds(ejercitoTipo.shootSpd);
                visible = false;
                this.GetComponent<Animator>().SetBool("visible", false);
                yield return new WaitForSeconds(ejercitoTipo.shootSpd);
                this.GetComponent<Animator>().SetBool("visible", true);
                yield return new WaitForSeconds(0.3f);
                visible = true;
                yield return new WaitForSeconds(ejercitoTipo.shootSpd);
                GameObject bala = ejercitoTipo.municion;
                for (int l = 0; l < municiones.Length; l++)
                {
                    if (!municiones[l].gameObject.activeInHierarchy)
                    {
                        bala = municiones[l].gameObject;
                        bala.GetComponent<proyectil>().bueno = false;
                        bala.GetComponent<proyectil>().dmg = this.ejercitoTipo.dmg + 1 * (this.vueltas - 1);
                        bala.transform.position = this.transform.position;
                        bala.SetActive(true);
                        bala.GetComponent<proyectil>().disparar();
                        this.GetComponent<GestorAudio>().reproducirAudio(0, 10);
                        break;
                    }
                }
            }
        }
        else if (this.ejercitoTipo.tipo == "de carga")
        {
            while (true)
            {
                yield return new WaitForSeconds(ejercitoTipo.shootSpd);
                GameObject enemigo = ejercitoTipo.municion;
                enemigo.GetComponent<Enemigo>().planeta = planeta;
                enemigo.transform.position = this.transform.position;
                GameObject.Instantiate(enemigo);
            }
        }
    }

    public void getDamage(int dmg)
    {
        if (!muerto)
        {
            this.GetComponent<GestorAudio>().reproducirAudio(1, 100);
            this.vida += -dmg;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "colisionPlaneta") enArea = true;
    }
}