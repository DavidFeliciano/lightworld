using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GestorAudio : MonoBehaviour
{
    public AudioClip[] sonidos;

    public void reproducirAudio(int nAudio, float vol)
    {
        this.GetComponent<AudioSource>().PlayOneShot(sonidos[nAudio], vol);
    }
}
