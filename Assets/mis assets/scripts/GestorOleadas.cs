using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GestorOleadas : MonoBehaviour
{
    public Oleada[] Oleadas;
    public GameObject[] EnemigosActivos;
    public GameObject[] nodosGeneracion;
    public proyectil[] proyectiles;
    public Planeta planeta;
    public PersistenciaSO persistente;
    public TextMeshProUGUI textoOleada;
    private int oleada;
    private int vueltas;

    void Start()
    {
        oleada = 1;
        vueltas = 1;
        if (persistente.Infinito) oleada = 0;
        StartCoroutine(generar());
    }

    public IEnumerator generar()
    {
        while (true)
        {
            for (int i = 0; i < Oleadas.Length; i++)
            {
                if (this.oleada != 0) 
                {
                    this.oleada = i + 1;
                    textoOleada.text = this.oleada + "";
                }
                EnemigosActivos = new GameObject[Oleadas[i].enemigos.Length];
                // hay que dejar descansar al jugador para que recoja recursos al principio
                yield return new WaitForSeconds(15);
                for (int l = 0; l < Oleadas[i].enemigos.Length; l++)
                {
                    GameObject enemigo = Oleadas[i].enemigos[l].gameObject;
                    enemigo.transform.position = nodosGeneracion[Random.Range(0, nodosGeneracion.Length)].transform.position;
                    enemigo.GetComponent<Enemigo>().planeta = planeta;
                    enemigo.GetComponent<Enemigo>().municiones = proyectiles;
                    enemigo.GetComponent<Enemigo>().vueltas = vueltas;
                    enemigo.GetComponent<Enemigo>().persistente = persistente;
                    EnemigosActivos[l] = Instantiate(enemigo);
                    yield return new WaitForSeconds(Oleadas[i].TempSpawn);
                }
                for (int l = 0; l < EnemigosActivos.Length; l++)
                {
                    while (true)
                    {
                        yield return null;
                        if (EnemigosActivos[l].GetComponent<Enemigo>().muerto && !EnemigosActivos[l].GetComponent<SpriteRenderer>().isVisible)
                        {
                            Destroy(EnemigosActivos[l].gameObject);
                            break;
                        }
                    }
                }
            }
            if (!this.persistente.Infinito) break;
            vueltas++;
        }
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("victoria");
    }
}