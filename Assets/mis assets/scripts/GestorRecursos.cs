using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GestorRecursos : MonoBehaviour
{
    public GameObject TextoMadera;
    public GameObject TextoPiedra;
    public GameObject TextoHierro;

    private int nMadera;
    private int nPiedra;
    private int nHierro;

    public GameObject TextoEscudo;
    public GameObject TextoGranja;
    public GameObject TextoCanon;
    public Planeta planeta;

    private int nGranja;
    private int nEscudo;
    private int nCanon;

    void Start()
    {
        nMadera = 1;
        nPiedra = 1;
        nHierro = 1;
        recalcularPrecios();
    }

    public void cojerMadera(int i)
    {
        nMadera += i;
        TextoMadera.GetComponent<TextMeshProUGUI>().text = nMadera + "";
    }

    public void cojerPiedra(int i)
    {
        nPiedra += i;
        TextoPiedra.GetComponent<TextMeshProUGUI>().text = nPiedra + "";
    }

    public void cojerHierro(int i)
    {
        nHierro += i;
        TextoHierro.GetComponent<TextMeshProUGUI>().text = nHierro + "";
    }

    public bool validar(int madera, int piedra, int hierro)
    {
        recalcularPrecios();
        if (madera<=nMadera && piedra<=nPiedra && hierro <= nHierro)
        {
            cojerMadera(madera * -1);
            cojerPiedra(piedra * -1);
            cojerHierro(hierro * -1);
            recalcularPrecios();
            return true;
        }
        StopAllCoroutines();
        StartCoroutine(fallo(madera, piedra, hierro));
        return false;
    }

    public bool validarConcreto(string tipoEdificio)
    {
        recalcularPrecios();
        int mult = 1;
        if (tipoEdificio == "Defensa") mult = nEscudo;
        else if (tipoEdificio == "Soporte") mult = nGranja;
        else if (tipoEdificio == "Ataque") mult = nCanon;
        if (mult <= nMadera && mult <= nPiedra && mult <= nHierro)
        {
            cojerMadera(mult * -1);
            cojerPiedra(mult * -1);
            cojerHierro(mult * -1);
            recalcularPrecios();
            return true;
        }
        StopAllCoroutines();
        StartCoroutine(fallo(mult, mult, mult));
        return false;
    }

    public void recalcularPrecios()
    {
        int[] valores = planeta.recalcularUnidades();
        nEscudo = valores[0] + 1;
        nGranja = valores[1] + 1;
        nCanon = valores[2] + 1;
        TextoEscudo.GetComponent<TextMeshProUGUI>().text = nEscudo + "";
        TextoGranja.GetComponent<TextMeshProUGUI>().text = nGranja + "";
        TextoCanon.GetComponent<TextMeshProUGUI>().text = nCanon + "";
    }

    public IEnumerator fallo(int madera, int piedra, int hierro)
    {
        if (!(madera <= nMadera)) TextoMadera.GetComponent<TextMeshProUGUI>().color = Color.red;
        if (!(piedra <= nPiedra)) TextoPiedra.GetComponent<TextMeshProUGUI>().color = Color.red;
        if (!(hierro <= nHierro)) TextoHierro.GetComponent<TextMeshProUGUI>().color = Color.red;
        yield return new WaitForSeconds(1);
        TextoMadera.GetComponent<TextMeshProUGUI>().color = Color.white;
        TextoPiedra.GetComponent<TextMeshProUGUI>().color = Color.white;
        TextoHierro.GetComponent<TextMeshProUGUI>().color = Color.white;
    }
}
