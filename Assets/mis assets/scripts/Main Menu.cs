using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public PersistenciaSO persistente;

    public void jugar()
    {
        persistente.Infinito = false;
        SceneManager.LoadScene("Game");
    }

    public void jugarInfinito()
    {
        persistente.Infinito = true;
        SceneManager.LoadScene("Game");
    }

    public void salir()
    {
        Application.Quit();
    }

    public void toMainMenu()
    {
        SceneManager.LoadScene("main menu");
    }

    public void recargar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void toCredits()
    {
        SceneManager.LoadScene("creditos");
    }
}
