using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class Manual : MonoBehaviour
{
    public int imagenActual = 0;
    public Texture2D[] ImagenesManual;

    void Start()
    {
        CambiarImagen(0);
    }

    public void CambiarImagen(int i)
    {
        imagenActual += i;
        if (imagenActual < 0) imagenActual = ImagenesManual.Length - 1;
        else if (imagenActual >= ImagenesManual.Length) imagenActual = 0;
        this.GetComponent<RawImage>().texture = ImagenesManual[imagenActual];
    }
}
