using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoverPan : MonoBehaviour
{
    public float spd = 5;

    private void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3((0 - this.transform.position.x), (0 - this.transform.position.y), 0).normalized * spd;
    }

    private void Update()
    {
        if (!this.GetComponent<SpriteRenderer>().isVisible && this.transform.position.x > 0) SceneManager.LoadScene("Lore");
    }
}
