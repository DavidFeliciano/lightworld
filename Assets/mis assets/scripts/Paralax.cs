using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Paralax : MonoBehaviour
{
    public GameObject objeto;

    void Update()
    {
        if (objeto != null) 
        {
            if (!objeto.activeSelf) this.GetComponent<RawImage>().material.mainTextureOffset = this.GetComponent<RawImage>().material.mainTextureOffset + new Vector2(0.0005f, 0);
        }
        else this.GetComponent<RawImage>().material.mainTextureOffset = this.GetComponent<RawImage>().material.mainTextureOffset + new Vector2(0.0005f, 0);
    }
}
