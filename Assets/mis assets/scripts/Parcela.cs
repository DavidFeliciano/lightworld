using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Parcela : MonoBehaviour
{
    public Planeta planeta;
    public bool ocupado;
    public bool creciendo;
    public EdificiosSO edificio;
    public EdificiosSO muerto;
    public int vida;
    public int vidaEscudo;
    public bool defensa;
    public bool died;
    public GestorRecursos GestorRecursos;
    public proyectil[] proyectiles;
    public GameObject ObjetoVida;

    // Sonidos gestor de audio: 0 disparar

    void Start()
    {
        ocupado = false;
        died = false;
        creciendo = false;
        ObjetoVida.GetComponent<Animator>().SetInteger("vida", vida);
    }

    public void recolectar()
    {
        if (edificio.Nombre == "Arbol") GestorRecursos.cojerMadera(1);
        else if (edificio.Nombre == "Piedra") GestorRecursos.cojerPiedra(1);
        else GestorRecursos.cojerHierro(1);
        ocupado = false;
        this.GetComponent<Animator>().runtimeAnimatorController = edificio.ANifVacio;
        this.GetComponent<SpriteRenderer>().sprite = edificio.SPifVacio;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "proyectil" && collision.gameObject.GetComponent<proyectil>().bueno == false)
        {
            getDamage(collision.gameObject.GetComponent<proyectil>().dmg);
            collision.gameObject.SetActive(false);
        }
    }

    public void cambio(EdificiosSO edificio)
    {
        this.edificio = edificio;
        defensa = false;
        this.GetComponent<Animator>().runtimeAnimatorController = edificio.ANifVacio;
        this.GetComponent<SpriteRenderer>().sprite = edificio.sprite;
        this.GetComponent<Animator>().runtimeAnimatorController = edificio.animator; 
        if (edificio.Tipo == "Defensa")
        {
            defensa = true;
            vidaEscudo = edificio.vida;
        }
        if (edificio.Tipo == "Soporte") planeta.recalcularTimepo(1);
        if (edificio.Tipo == "Ataque") StartCoroutine(disparar());
        if (edificio.Tipo == "Sanador") StartCoroutine(regenerar());
        if (edificio.Tipo == "Recurso") StartCoroutine(crecerRecurso());
        else ocupado = true;
    }

    public void demoler()
    {
        ocupado = false;
        this.GetComponent<Animator>().runtimeAnimatorController = edificio.ANifVacio;
        this.GetComponent<SpriteRenderer>().sprite = edificio.SPifVacio;
    }

    public IEnumerator disparar ()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (edificio.Tipo != "Ataque" || !ocupado) break;
            GameObject bala = edificio.municion;
            for (int l = 0; l < proyectiles.Length; l++)
            {
                if (!proyectiles[l].gameObject.activeInHierarchy)
                {
                    bala = proyectiles[l].gameObject;
                    bala.GetComponent<proyectil>().bueno = true;
                    bala.GetComponent<proyectil>().dmg = 1;
                    bala.transform.position = this.transform.position;
                    bala.SetActive(true);
                    bala.GetComponent<proyectil>().disparar();
                    this.GetComponent<GestorAudio>().reproducirAudio(0, 10);
                    break;
                }
            }
        }
    }

    public void getDamage(int dmg)
    {
        if (defensa)
        {
            this.vidaEscudo += dmg * -1;
            if (this.vidaEscudo <= 0) 
            {
                if (this.vidaEscudo < 0) 
                {
                    this.vida += this.vidaEscudo;
                }
                this.defensa = false;
                this.GetComponent<SpriteRenderer>().sprite = edificio.SPifVacio;
                this.ocupado = false;
            }
        }
        else
        {
            this.vida += dmg * -1;
            ObjetoVida.GetComponent<Animator>().SetInteger("vida", vida);
        }
        if (this.vida <= 0)
        {
            cambio(muerto);
            this.died = true;
            StartCoroutine(planeta.mori());
        }
    }

    public IEnumerator crecerRecurso()
    {
        creciendo = true;
        ocupado = false;
        this.GetComponent<Animator>().runtimeAnimatorController = edificio.animator;
        yield return new WaitForSeconds(4);
        ocupado = true;
        creciendo = false;
    }

    public void revivir() 
    {
        this.vida = 1;
        this.died = false;
        this.ocupado = false;
    }

    public IEnumerator regenerar()
    {
        while (this.vida != 20)
        {
            yield return new WaitForSeconds(0.5f);
            this.vida++;
            ObjetoVida.GetComponent<Animator>().SetInteger("vida", vida);
        }
        yield return new WaitForSeconds(0.5f);
        this.demoler();
    }
}