using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public class Pibe : MonoBehaviour
{

    public List<Parcela> parcelasList;
    public RotarDer RotarDer;
    public RotarIz RotarIz;
    public bool quieto;
    public GameObject barra;
    private float barraX;
    public bool enCorrutine;

    private void Start()
    {
        barraX = barra.GetComponent<RectTransform>().sizeDelta.x;
        barra.SetActive(false);
        enCorrutine = false;
    }

    private void Update()
    {
        if (RotarDer.Encima == true || RotarIz.Encima == true)
        {
            this.GetComponent<Animator>().SetBool("moviendose", true);
            if (RotarIz.Encima == true) this.GetComponent<SpriteRenderer>().flipX = false;
            else if (RotarDer.Encima == true) this.GetComponent<SpriteRenderer>().flipX = true;
            quieto = false;
        }
        else
        {
            this.GetComponent<Animator>().SetBool("moviendose", false);
            quieto = true;
        }
        if (quieto && !enCorrutine && parcelasList[0].ocupado && parcelasList[0].edificio.Tipo == "Recurso" && !parcelasList[0].creciendo) StartCoroutine(talar());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "parcela")
        {
            parcelasList.Add(collision.GetComponent<Parcela>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "parcela")
        {
            parcelasList.Remove(collision.GetComponent<Parcela>());
        }
    }

    public void construir (EdificiosSO edificio)
    {
        if (parcelasList.Count != 0 && !parcelasList[0].ocupado && !parcelasList[0].died) parcelasList[0].cambio(edificio);
    }

    public void construirRegenerador(EdificiosSO edificio)
    {
        if (parcelasList.Count != 0 && parcelasList[0].died)
        {
            parcelasList[0].revivir();
            parcelasList[0].cambio(edificio);
        }
    }

    public void demoler()
    {
        if (parcelasList.Count != 0 && parcelasList[0].ocupado && parcelasList[0].edificio.Tipo != "Recurso") parcelasList[0].demoler();
    }

    public IEnumerator talar() 
    {
        this.GetComponent<Animator>().SetBool("picar", true);
        enCorrutine = true;
        float size = barraX;
        barra.SetActive(true);
        barra.GetComponent<RectTransform>().sizeDelta = new Vector3(size, barra.GetComponent<RectTransform>().sizeDelta.y, 0);
        while (quieto)
        {
            yield return new WaitForSeconds(0.2f);
            size += -50;
            barra.GetComponent<RectTransform>().sizeDelta = new Vector3(size, barra.GetComponent<RectTransform>().sizeDelta.y, 0);
            if (size <= 0)
            {
                parcelasList[0].recolectar();
                break;
            }
        }
        barra.SetActive(false);
        enCorrutine = false;
        this.GetComponent<Animator>().SetBool("picar", false);
    }

    public bool validarParcelaVacia()
    {
        if (parcelasList[0].edificio.Nombre == "Vacio" || parcelasList[0].ocupado == false) return true;
        return false;
    }

    public bool validarParcelaOcupada()
    {
        if (parcelasList[0].edificio.Nombre == "Canon" || parcelasList[0].edificio.Nombre == "Escudo" || parcelasList[0].edificio.Nombre == "Granja") return true;
        return false;
    }

    public bool validarParcelaMuerta()
    {
        return parcelasList[0].died;
    }
}
