using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Planeta : MonoBehaviour
{
    public Parcela[] parcelas;
    public EdificiosSO[] recurso;
    public float genTemp;
    private int nGranjas;
    public GestorRecursos gestRecursos;
    public GameObject objeto;

    void Start()
    {
        StartCoroutine(generarRecurso());
        nGranjas = 0;
        recalcularTimepo(0);
    }

    public IEnumerator generarRecurso()
    {
        while (true)
        {
            int rand = Random.Range(0, parcelas.Length);
            if (parcelas[rand].ocupado == false && parcelas[rand].creciendo == false)
            {
                parcelas[rand].ocupado = true;
                parcelas[rand].cambio(this.recurso[Random.Range(0, recurso.Length)]);
                yield return new WaitForSeconds(genTemp);
            }
            yield return null;
        }
    }

    public void recalcularTimepo(int val)
    {
        nGranjas += val;
        genTemp = 8 - (nGranjas * 2f);
        if (genTemp <= 1) genTemp = 1;
    }

    public IEnumerator mori()
    {
        gestRecursos.recalcularPrecios();
        int count = 0;
        for (int i=0; i<parcelas.Length; i++)
        {
            if (parcelas[i].died) count++;
        }
        if (count == parcelas.Length)
        {
            yield return new WaitForSeconds(1.5f);
            SceneManager.LoadScene("game over");
        }
    }

    public int[] recalcularUnidades()
    {
        int[] num = new int[3];
        num[0] = 0;
        num[1] = 0;
        num[2] = 0;
        for (int i = 0; i < parcelas.Length; i++)
        {
            if (parcelas[i].ocupado && parcelas[i].edificio.Tipo== "Defensa") num[0]++;
            else if (parcelas[i].ocupado && parcelas[i].edificio.Tipo == "Soporte") num[1]++;
            else if (parcelas[i].ocupado && parcelas[i].edificio.Tipo == "Ataque") num[2]++;
        }
        return num; // 0 defensa 1 granja 2 ataque
    }
}