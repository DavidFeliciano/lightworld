using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarIz : MonoBehaviour
{
    public Planeta Lightworld;
    public bool Encima;

    void Start()
    {
        Encima = false;
    }

    void Update()
    {
        if (Encima)
        {
            Lightworld.gameObject.transform.Rotate(new Vector3(0, 0, 1.5f));
        }
    }

    public void OnMouseEnter()
    {
        Encima = true;
    }

    public void OnMouseExit()
    {
        Encima = false;
    }
}
