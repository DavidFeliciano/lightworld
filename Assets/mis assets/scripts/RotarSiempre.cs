using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarSiempre : MonoBehaviour
{
    public GameObject objeto;
    public float spd = 1;
    void Update()
    {
        if (objeto != null)
        {
            if (!objeto.activeSelf) this.gameObject.transform.Rotate(new Vector3(0, 0, -0.2f) * spd);
        }
        else this.gameObject.transform.Rotate(new Vector3(0, 0, -0.2f) * spd);
    }
}
