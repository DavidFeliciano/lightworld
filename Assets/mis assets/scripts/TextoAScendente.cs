using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TextoAScendente : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        this.GetComponent<RectTransform>().anchoredPosition = this.GetComponent<RectTransform>().anchoredPosition + new Vector2(0, 1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "colisionPlaneta") SceneManager.LoadScene("main menu");
    }
}
