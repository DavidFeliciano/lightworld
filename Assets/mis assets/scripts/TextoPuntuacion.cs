using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextoPuntuacion : MonoBehaviour
{
    public PersistenciaSO persistente;

    private void Start()
    {
        persistente.punt = 0;
    }

    void Update()
    {
        this.GetComponent<TextMeshProUGUI>().text = persistente.punt + "";
    }
}
