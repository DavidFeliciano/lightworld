using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botonConstruir : MonoBehaviour
{
    public Pibe pibe;
    public EdificiosSO edificio;
    public GestorRecursos GestorRecursos;

    public void construir()
    {
        if (pibe.validarParcelaVacia()) if (GestorRecursos.validarConcreto(edificio.Tipo)) pibe.construir(edificio);
        GestorRecursos.recalcularPrecios();
    }

    public void demoler()
    {
        if (pibe.validarParcelaOcupada()) if (GestorRecursos.validar(1, 1, 1)) pibe.demoler();
        GestorRecursos.recalcularPrecios();
    }

    public void regenerador()
    {
        if (pibe.validarParcelaMuerta()) if (GestorRecursos.validar(5, 5, 5)) pibe.construirRegenerador(edificio);
        GestorRecursos.recalcularPrecios();
    }
}