using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class menudepausa : MonoBehaviour
{
    public Canvas pausa;
    public GameObject boton;
    public bool ManipularTiempo;

    public void Start()
    {
        this.despausar();
    }

    public void pausar()
    {
        if (ManipularTiempo) Time.timeScale = 0;
        pausa.gameObject.SetActive(true);
        boton.gameObject.SetActive(false);
    }

    public void despausar()
    {
        if (ManipularTiempo) Time.timeScale = 1;
        pausa.gameObject.SetActive(false);
        boton.gameObject.SetActive(true);
    } 
}
