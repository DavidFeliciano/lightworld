using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class menudesplegable : MonoBehaviour
{
    public RectTransform elemento;
    public RectTransform elementoReferencia;
    public TextMeshProUGUI textoBoton;
    private Vector2 poosicionInicial;
    private bool moviendose = false;

    void Start()
    {
        poosicionInicial = elemento.position;
    }

    public void moverse()
    {
        moviendose = !moviendose;
        if (moviendose)
        {
            elemento.position = new Vector2(poosicionInicial.x, elementoReferencia.position.y);
            textoBoton.text = "v";
        }
        else
        {
            elemento.position = poosicionInicial;
            textoBoton.text = "^";
        } 
    }
}
