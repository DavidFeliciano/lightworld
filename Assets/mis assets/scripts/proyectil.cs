using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectil : MonoBehaviour
{
    public bool bueno;
    public int dmg;
    private Enemigo enemigoCapturado;

    private void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void disparar()
    {
        enemigoCapturado = null;
        int mult = 1;
        if (bueno) mult = -1;
        this.GetComponent<Rigidbody2D>().velocity = new Vector3((0 - this.transform.position.x), (0.48f - this.transform.position.y), 0).normalized * 15 * mult;
    }

    void Update()
    {
        if (this.GetComponent<SpriteRenderer>().isVisible == false) this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "enemigo")
        {
            if (bueno && enemigoCapturado == null)
            {
                enemigoCapturado = collision.GetComponent<Enemigo>();
                enemigoCapturado.getDamage(1);
                this.gameObject.SetActive(false);
            }
        }
    }
}
